class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.references :user, foreign_key: {on_delete: :cascade, on_update: :cascade}
      t.string :title, index: true
      t.text :body, index: true

      t.timestamps
    end
  end
end
