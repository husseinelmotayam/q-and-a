class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.references :user, foreign_key: {on_delete: :cascade, on_update: :cascade}
      t.references :question, foreign_key: {on_delete: :cascade, on_update: :cascade}
      t.text :body

      t.timestamps
    end
  end
end
