# == Route Map
#
#                                Prefix Verb     URI Pattern                                        Controller#Action
#                                  root GET      /                                                  home#index
#                      question_answers GET      /questions/:question_id/answers(.:format)          answers#index
#                                       POST     /questions/:question_id/answers(.:format)          answers#create
#                   new_question_answer GET      /questions/:question_id/answers/new(.:format)      answers#new
#                  edit_question_answer GET      /questions/:question_id/answers/:id/edit(.:format) answers#edit
#                       question_answer GET      /questions/:question_id/answers/:id(.:format)      answers#show
#                                       PATCH    /questions/:question_id/answers/:id(.:format)      answers#update
#                                       PUT      /questions/:question_id/answers/:id(.:format)      answers#update
#                                       DELETE   /questions/:question_id/answers/:id(.:format)      answers#destroy
#                             questions GET      /questions(.:format)                               questions#index
#                                       POST     /questions(.:format)                               questions#create
#                          new_question GET      /questions/new(.:format)                           questions#new
#                         edit_question GET      /questions/:id/edit(.:format)                      questions#edit
#                              question GET      /questions/:id(.:format)                           questions#show
#                                       PATCH    /questions/:id(.:format)                           questions#update
#                                       PUT      /questions/:id(.:format)                           questions#update
#                                       DELETE   /questions/:id(.:format)                           questions#destroy
# user_google_oauth2_omniauth_authorize GET|POST /users/auth/google_oauth2(.:format)                users/omniauth_callbacks#passthru
#  user_google_oauth2_omniauth_callback GET|POST /users/auth/google_oauth2/callback(.:format)       users/omniauth_callbacks#google_oauth2
#                      new_user_session GET      /signin(.:format)                                  home#index
#                  destroy_user_session DELETE   /signout(.:format)                                 devise/sessions#destroy
#

Rails.application.routes.draw do
  root to: "home#index"

  resources :questions do
    resources :answers
  end

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }, skip: [:sessions]
  as :user do
    get 'signin', to: 'home#index', as: :new_user_session    
    delete 'signout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
