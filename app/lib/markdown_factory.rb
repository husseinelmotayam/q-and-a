class MarkdownFactory
  
  def self.instance
    if !@markdown
      @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true, strikethrough: true, superscript: true, underline: true, highlight: true, quote: true, footnotes: true)
    end
    @markdown
  end
end